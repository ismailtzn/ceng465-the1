#!/usr/bin/python3
import sys
import copy
GAP = -2
MATCH = 4
MISMATCH = -3

# open files and read them.
with open(sys.argv[1], 'r') as g1File:
    g1 = g1File.readline().strip(' \t\n\r')
with open(sys.argv[2], 'r') as g2File:
    g2 = g2File.readline().strip(' \t\n\r')
with open(sys.argv[3], 'r') as rsFile:
    rs = rsFile.readline().strip(' \t\n\r')


# I assumed that we won't use this for protein sequences.
alphabet = ["A", "T", "G", "C"]

# These my first attempts
#
# def getVector(genom, k, vBase):
#     vgs = []
#     vg = copy.deepcopy(vBase)
#     lenG = len(genom)
#     for i in range(k-2):
#         vg[genom[i:i+3]] += 1
#     vgs.append(vg)
#
#     for i in range(0, lenG - k):
#         vg = copy.deepcopy(vgs[i])
#         vg[genom[i:i+3]] -= 1
#         vg[genom[i+k-2:i+k+1]] += 1
#         vgs.append(vg)
#     return vgs
#
# def getDistance(vrs, vgxi):
#     return sum([abs(v - vgxi[k]) for k,v in vrs.items()])



def getVbase():
    vBase = {}
    for a1 in alphabet:
        for a2 in alphabet:
            for a3 in alphabet:
                vBase[a1 + a2 + a3] = 0
    return vBase

vBase = getVbase()
k = len(rs)
vrs = copy.deepcopy(vBase)
for i in range(k-2):
    vrs[rs[i:i+3]] += 1


def getDistanceList(genom, k, vBase, referance, vrs):
    distanceList = []
    vg = copy.deepcopy(vBase)
    lenG = len(genom)
    # print(genom)
    for i in range(k-2):
        vg[genom[i:i+3]] += 1

    distance = sum([abs(value - vg[key]) for key, value in vrs.items()])
    distanceList.append(distance)
    for i in range(0, lenG - k):
        vg[genom[i:i+3]] -= 1
        vg[genom[i+k-2:i+k+1]] += 1
        distance = sum([abs(value - vg[key]) for key, value in vrs.items()])
        distanceList.append(distance)

    return distanceList

def getMinIndex(l):
    i = 0
    j = 0
    for val in l:
        if val < l[i]:
            i = j
        j +=1
    return i
# print(getVector(g1, k, vBase))
# vg1 = getVector(g1, k, vBase)
# print(getDistance(vrs, vg1[0]))
# print(getDistanceList(g1, k, vBase, rs, vrs))
distanceList1 = getDistanceList(g1, k, vBase, rs, vrs)
distanceList2 = getDistanceList(g2, k, vBase, rs, vrs)
min1 = getMinIndex(distanceList1)
min2 = getMinIndex(distanceList2)
A = g1[min1:min1 + k]
B = g2[min2:min2 + k]

# print(A)
# print(B)

# I wrote the following code by inspired from the c file that shared on the course page.



def m(a, b):
    if (a == b):
        return MATCH
    return MISMATCH

def sMax(a, b, c):
    if (a >= b and a >= c):
        return a
    elif (b >= a and b >= c):
        return b
    else:
        return c


def tMax(a, b, c):
    if (a >= b and a >= c):
        return 'D'
    elif (b >= a and b >= c):
        return 'L'
    else:
        return 'U'

s = []
t = []
for y in range(k+1):
    s.append([0] * (k+1))
    t.append([0] * (k+1))

t[0][0] = "D"
# s = [[0] * (k+1)] * (k+1)
# t = [["D"] * (k+1)] * (k+1)

i = 0
while i <= k:
    s[i][0] = GAP * i
    t[i][0] = "U"
    i += 1

i = 0
while i <= k:
    s[0][i] = GAP * i
    t[0][i] = "L"
    i += 1


i = 1
while i <= k :
    j = 1
    while j <= k:
        s[i][j] = sMax(s[i-1][j-1] + m(A[i-1], B[j-1]), s[i][j-1] + GAP, s[i-1][j] + GAP)
        t[i][j] = tMax(s[i-1][j-1] + m(A[i-1], B[j-1]), s[i][j-1] + GAP, s[i-1][j] + GAP)
        j += 1
    i += 1

print("The score of the alignment is : {}".format(s[k][k]))
# print(A)
# print(B)
# print(s)
# print(t)

i = k
j = k
RA = ""
RB = ""
RM = ""
while (i != 0 and j != 0):
    if (t[i][j] == 'D'):
        RA = A[i - 1] + RA
        RB = B[j - 1] + RB
        if (A[i - 1] == B[j - 1]):
            RM = '|' + RM
        else:
            RM = '*' + RM
        i -= 1
        j -= 1
    elif (t[i][j] == 'L'):
        RA = '-' + RA
        RB = B[j - 1] + RB
        RM = ' ' + RM
        j -= 1
    elif (t[i][j] == 'U'):
        RA = A[i - 1] + RA
        RB = '-' + RB
        RM = ' ' + RM
        i -= 1

for h in range(i):
    RA = A[i - h - 1] + RA
    RB = "-" + RB
    RM = " " + RM
for h in range(j):
    RA = "-" + RA
    RB = B[j - h - 1] + RB
    RM = " " + RM

print(RA)
print(RM)
print(RB)
